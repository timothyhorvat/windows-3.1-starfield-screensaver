﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class CustomButton : Button
{
    private const float _buttonHeldTimerOffset = 0.3f;
    private bool _buttonDown;
    private Coroutine _buttonHeldCoroutine;

    /// <summary>
    /// In order to allow the user to hold down the button, I've had to override the original functionality of some of the mouse buttons.
    /// Assign _buttonDown as true and start the coroutine to listen if the mouse button remains down.
    /// </summary>
    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        if(_buttonHeldCoroutine != null)
        {
            StopCoroutine(_buttonHeldCoroutine);
        }
        _buttonHeldCoroutine = StartCoroutine(ButtonHeld());
    }

    /// <summary>
    /// Assign _buttonDown as false to prevent the repetitve invoke of 'OnClick()' in the ButtonHeld Coroutine.
    /// </summary>
    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        if(_buttonDown)
        {
            _buttonDown = false;
        }
    }


    /// <summary>
    /// Waits the designated time before Invoking the Action assigned to 'OnClick()' while the mouse button is down.
    /// </summary>
    private IEnumerator ButtonHeld()
    {
        _buttonDown = true;
        yield return new WaitForSeconds(_buttonHeldTimerOffset);

        while (_buttonDown)
        {
            onClick.Invoke();
            yield return new WaitForSeconds(0.01f);
        }
    }
}
