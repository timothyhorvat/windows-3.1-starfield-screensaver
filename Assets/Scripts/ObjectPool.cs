﻿using UnityEngine;
using System.Collections.Generic;
using StarfieldScreensaver;

namespace SimpleObjectPool
{
    public class ObjectPool
    {
        private const int MaxPoolSize = 1000;

        private Transform _activeStars;
        private Transform _inactiveStars;
        private GameObject _starPrefab;

        private int _currentActiveStars;

        private Stack<GameObject> _pool;

        /// <summary>
        /// Passing the variables through a constructor allows this script to be created when need, instead of being assigned to a GameObject.
        /// Also initialises the Stack.
        /// </summary>
        public ObjectPool(Transform activeStars, Transform inactiveStars, GameObject starPrefab)
        {
            _activeStars = activeStars;
            _inactiveStars = inactiveStars;
            _starPrefab = starPrefab;

            _pool = new Stack<GameObject>();
        }

        /// <summary>
        /// This is called when the User input their own variable for Active Stars on the Performance Tracker
        /// </summary>
        public void UpdateActiveStars(string newAmount)
        {
            UpdateActiveStars(int.Parse(newAmount));
        }

        /// <summary>
        /// Allows for the application to specify how many stars are to be created without a String variable.
        /// </summary>
        public void UpdateActiveStars(int newAmount)
        {
            int newStars = Mathf.Clamp(newAmount, 0, newAmount);
            int diff = Mathf.Abs(newStars - _currentActiveStars);

            for (int i = 0; i < diff; i++)
            {
                if (newStars >= _currentActiveStars)
                {
                    IncreaseStars();
                }
                else
                {
                    DecreaseStars();
                }
            }
        }

        /// <summary>
        /// Deactivates a star and places it under InactiveStars in the hierarchy.
        /// </summary>
        public void DecreaseStars()
        {
            GameObject star = _activeStars.GetChild(0).gameObject;
            star.transform.parent = _inactiveStars;
            star.SetActive(false);
            _pool.Push(star);
            RefreshActiveStarsCount();
            CleanUpObjectPool();
        }

        /// <summary>
        /// Activates a star from the object pool or instantiates a new one, and places it under ActiveStars in the hierarchy.
        /// </summary>
        public void IncreaseStars()
        {
            if (_inactiveStars.childCount > 0)
            {
                GameObject star = _pool.Pop();
                star.transform.parent = _activeStars;
                star.gameObject.SetActive(true);
            }
            else
            {
                Object.Instantiate(_starPrefab, _activeStars);
            }
            RefreshActiveStarsCount();
        }

        /// <summary>
        /// Removes old instances from the pool if the number stored is greater than the defined capacity.
        /// </summary>
        private void CleanUpObjectPool()
        {
            if (_pool.Count > MaxPoolSize)
            {
                Object.Destroy(_pool.Pop());
            }
        }

        /// <summary>
        /// Updates the CurrentActiveStars displayed on the Performance Tracker.
        /// </summary>
        private void RefreshActiveStarsCount()
        {
            _currentActiveStars = _activeStars.childCount;
            GameManager.Instance.RefreshActiveStarsCount();
        }
    }
}