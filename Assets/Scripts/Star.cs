﻿using UnityEngine;
using System.Collections;

namespace StarfieldScreensaver
{
    public class Star : MonoBehaviour
    {
        private const float _initSpeed = .5f;
        private const float _initSize = .1f;

        private float _currentSpeed;
        private float _currentSize;
        private float _sizeMultiplyer;

        private Vector3 _direction;
        private Transform _star;
        private CameraBounds _cameraBounds;
        private Coroutine _moveStar;


        /// <summary>
        /// Resets and Starts the star's variables and movement.
        /// </summary>
        private void OnEnable()
        {
            if (_star == null)
            {
                _star = transform;
            }
            _cameraBounds = GameManager.Instance.CamBounds;
            Initialise();

            if (_moveStar != null)
            {
                StopCoroutine(_moveStar);
            }
            _moveStar = StartCoroutine(MoveStar());
        }

        /// <summary>
        /// Resets the star's speed and size.
        /// Assigns random _sizeMultiplyer.
        /// Calculates the stars direction to move.
        /// </summary>
        private void Initialise()
        {
            AssignPosition();
            _currentSpeed = _initSpeed;
            _currentSize = _initSize;

            _sizeMultiplyer = Random.Range(3f, 12f);


            _star.localScale = Vector3.one * _initSize;
            _direction = _star.position.normalized;
        }

        /// <summary>
        /// Using the camera's view bounds, The star is assigned a position.
        /// </summary>
        private void AssignPosition()
        {
            Vector2 StartPos = new Vector2(
                Random.Range(_cameraBounds.MinBounds.x, _cameraBounds.MaxBounds.x),
                Random.Range(_cameraBounds.MinBounds.y, _cameraBounds.MaxBounds.y));

            transform.position = StartPos;
        }

        /// <summary>
        /// A Coroutine is used instead of Update() as the star might be deactived and placed in the ObjectPool.
        /// When the star is deactivated, the Coroutine escapes the loop and finishes.
        /// </summary>
        /// <returns></returns>
        private IEnumerator MoveStar()
        {
            while (gameObject.activeSelf)
            {
                _currentSpeed += Time.deltaTime;
                _star.position += _direction * _currentSpeed * Time.deltaTime;

                _currentSize += Time.deltaTime / _sizeMultiplyer;
                _star.localScale = Vector3.one * _currentSize;

                IsOffScreen();

                yield return null;
            }
        }

        /// <summary>
        /// Check to see if the star's position is outside of the camera's view.
        /// </summary>
        private void IsOffScreen()
        {
            if (transform.position.x < _cameraBounds.MinBounds.x || transform.position.x > _cameraBounds.MaxBounds.x ||
                transform.position.y < _cameraBounds.MinBounds.y || transform.position.y > _cameraBounds.MaxBounds.y)
            {
                Initialise();
            }
        }
    }
}