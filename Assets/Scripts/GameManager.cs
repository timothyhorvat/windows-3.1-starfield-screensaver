﻿using UnityEngine;
using UnityEngine.UI;
using SimpleObjectPool;

namespace StarfieldScreensaver
{
    public class GameManager : MonoBehaviour
    {
        #region UI Elements

        public Text FPSText;
        public InputField ActiveObjectsText;
        public Button DecreaseActiveStars;
        public Button IncreaseActiveStars;

        #endregion

        #region Unity Objects

        public Transform ActiveStars;
        public Transform InactiveStars;
        public GameObject StarPrefab;

        #endregion

        #region Public Variables
        public static GameManager Instance { get; private set; }
        public CameraBounds CamBounds { get; private set; }
        public float FPSUpdateInterval = 0.5F;
        #endregion

        #region Private Variables
        private float accumFPS;
        private int totalFrames;
        private float nextFPSUpdate;

        private ObjectPool objPool;
        #endregion

        #region Unity Event Handlers

        /// <summary>
        /// Setup the scene;
        ///  - Create the ObjectPool
        ///  - Assign UI element listeners
        ///  - Initiate the Stars in the scene.
        /// </summary>
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }

            objPool = new ObjectPool(ActiveStars, InactiveStars, StarPrefab);

            ActiveObjectsText.onEndEdit.AddListener(objPool.UpdateActiveStars);
            DecreaseActiveStars.onClick.AddListener(objPool.DecreaseStars);
            IncreaseActiveStars.onClick.AddListener(objPool.IncreaseStars);

            CalculateCameraBounds();
            objPool.UpdateActiveStars(3000);
            RefreshActiveStarsCount();

        }

        /// <summary>
        /// Calculate the framerate of the scene/application and display on the Performance Tracker.
        /// </summary>
        private void Update()
        {
            nextFPSUpdate -= Time.deltaTime;
            accumFPS += Time.timeScale / Time.deltaTime;
            ++totalFrames;

            // Interval ended - update GUI text and start new interval
            if (nextFPSUpdate <= 0.0)
            {
                float fps = accumFPS / totalFrames;
                nextFPSUpdate = FPSUpdateInterval;
                accumFPS = 0f;
                totalFrames = 0;

                FPSText.text = fps.ToString("N2");
            }
        }

        #endregion

        #region Misc.

        /// <summary>
        /// Updates the Active Stars InputField on the Performance Tracker to allow the user to see how many stars are currently on screen.
        /// </summary>
        public void RefreshActiveStarsCount()
        {
            ActiveObjectsText.text = ActiveStars.childCount.ToString();
        }
        
        /// <summary>
        /// Calculate the camera's bounds here for the active stars to use.
        /// </summary>
        private void CalculateCameraBounds()
        {
            float vert = Camera.main.orthographicSize;
            float horz = vert * Screen.width / Screen.height;

            //the CameraBounds are represented as two points;
            // - Bottom Left
            // - Top Right
            CamBounds = new CameraBounds()
            {
                MinBounds = new Vector2(-horz, -vert),
                MaxBounds = new Vector2(horz, vert)
            };
        }

        #endregion
    }

    public struct CameraBounds
    {
        public Vector2 MinBounds;
        public Vector2 MaxBounds;
    }
}
